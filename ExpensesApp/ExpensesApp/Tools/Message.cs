﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ExpensesApp.Tools
{
    public static class Message
    {
        public static async Task EnviarAlert(string mensaje, string ok)
        {

            await App.Current.MainPage.DisplayAlert("ExpenseApp", mensaje, ok);

        }

        public static async Task<bool> EnviarAlert(string mensaje, string ok, string cancel)
        {
            return await App.Current.MainPage.DisplayAlert("ExpenseApp", mensaje, ok, cancel);
        }
    }
}

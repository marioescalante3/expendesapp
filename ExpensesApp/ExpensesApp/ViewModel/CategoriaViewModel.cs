﻿using ExpensesApp.DAL;
using ExpensesApp.Entidades;
using ExpensesApp.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace ExpensesApp.ViewModel
{
    public class CategoriaViewModel : BaseViewModel
    {
        #region Commands

        public ICommand CrearCommand { get; set; }
        public ICommand EliminarCommand { get; set; }
        public ICommand ModificarCommand { get; set; }

        #endregion
        #region FullProperties

        private List<CategoriaBase> datos;

        public List<CategoriaBase> Datos
        {
            get { return datos; }
            set { SetProperty(ref datos, value); }
        }

        private CategoriaBase categoriaSeleccionada;

        public CategoriaBase CategoriaSeleccionada
        {
            get { return categoriaSeleccionada; }
            set { SetProperty(ref categoriaSeleccionada, value); }
        }


        #endregion
        #region Variables
        GenericRepository<CategoriaGastos> _categoriaGastos;
        GenericRepository<CategoriaEntradas> _categoriaEntradas;
        #endregion

        public CategoriaViewModel(string categoria)
        {
            CrearCommand = new Command(CrearCommandExecute);
            EliminarCommand = new Command(EliminarCommandExecute);
            ModificarCommand = new Command(ModificarCommandExecute);
            _categoriaEntradas = new GenericRepository<CategoriaEntradas>();
            _categoriaGastos = new GenericRepository<CategoriaGastos>();
            CategoriaSeleccionada = new CategoriaBase();
            Title = categoria;
            ActualizarLista();
        }

        #region Metodos

        private async void CrearCommandExecute()
        {
            if (Title == "Entradas")
            {
                if (_categoriaEntradas.Create(new CategoriaEntradas() { Color= CategoriaSeleccionada.Color, Name= CategoriaSeleccionada.Name }) != null)
                {
                    await Message.EnviarAlert("Categoria creada correctamente", "Aceptar");
                }
                else
                {
                    await Message.EnviarAlert($"Error al crear categoria.", "Aceptar");
                }
            }
            else
            {
                if (_categoriaGastos.Create(new CategoriaGastos() { Color=CategoriaSeleccionada.Color, Name=CategoriaSeleccionada.Name }) != null)
                {
                    await Message.EnviarAlert("Categoria creada correctamente", "Aceptar");
                }
                else
                {
                    await Message.EnviarAlert($"Error al crear categoria.", "Aceptar");
                }
            }
            ActualizarLista();
        }

        private async void EliminarCommandExecute()
        {
            if (CategoriaSeleccionada!=null)
            {
                if (Title == "Entradas")
                {
                    if (_categoriaEntradas.Delete(CategoriaSeleccionada.Id))
                    {
                        await Message.EnviarAlert("Categoria eliminada correctamente", "Aceptar");
                    }
                    else
                    {
                        await Message.EnviarAlert($"Error al eliminar categoria.", "Aceptar");
                    }
                }
                else
                {
                    if (_categoriaGastos.Delete(CategoriaSeleccionada.Id))
                    {
                        await Message.EnviarAlert("Categoria eliminada correctamente", "Aceptar");
                    }
                    else
                    {
                        await Message.EnviarAlert($"Error al eliminar categoria.", "Aceptar");
                    }
                }
            }
            ActualizarLista();
        }

        private async void ModificarCommandExecute()
        {
            if (Title == "Entradas")
            {
                if (_categoriaEntradas.Update(new CategoriaEntradas() { Id=CategoriaSeleccionada.Id, FechaHoraCreacion=categoriaSeleccionada.FechaHoraCreacion, Name=CategoriaSeleccionada.Name, Color=CategoriaSeleccionada.Color }) != null)
                {
                    await Message.EnviarAlert("Categoria modificada correctamente", "Aceptar");
                }
                else
                {
                    await Message.EnviarAlert($"Error al modificar categoria.", "Aceptar");
                }
            }
            else
            {
                if (_categoriaGastos.Update(new CategoriaGastos() { Id = CategoriaSeleccionada.Id, FechaHoraCreacion = categoriaSeleccionada.FechaHoraCreacion, Name = CategoriaSeleccionada.Name, Color = CategoriaSeleccionada.Color }) != null)
                {
                    await Message.EnviarAlert("Categoria modificada correctamente", "Aceptar");
                }
                else
                {
                    await Message.EnviarAlert($"Error al modificar categoria.", "Aceptar");
                }
            }
            ActualizarLista();
        }

        internal void ActualizarLista()
        {

            if (Title == "Entradas")
            {
                Datos = _categoriaEntradas.Read.ToList<CategoriaBase>();
            }
            else
            {
                Datos = _categoriaGastos.Read.ToList<CategoriaBase>();
            }
        }
        #endregion
    }
}

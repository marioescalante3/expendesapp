﻿using ExpensesApp.DAL;
using ExpensesApp.Entidades;
using ExpensesApp.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace ExpensesApp.ViewModel
{
    public class ActivoPasivoViewModel:BaseViewModel
    {
        #region Commands

        public ICommand CrearCommand { get; set; }
        public ICommand ModificarCommand { get; set; }
        public ICommand EliminarCommand { get; set; }
        public ICommand BuscarCommand { get; set; }



        #endregion
        #region FullProperties

        private List<ActivoPasivoBase> datos;

        public List<ActivoPasivoBase> Datos
        {
            get { return datos; }
            set {SetProperty(ref datos , value); }
        }
        private ActivoPasivoBase datoSelect;

        public ActivoPasivoBase DatoSelect
        {
            get { return datoSelect; }
            set {SetProperty(ref datoSelect , value); }
        }

        private List<CategoriaBase> categorias;

        public List<CategoriaBase> Categorias
        {
            get { return categorias; }
            set {SetProperty(ref categorias , value); }
        }
        private CategoriaBase categoriaSelect;

        public CategoriaBase CategoriaSelect
        {
            get { return categoriaSelect; }
            set {SetProperty(ref categoriaSelect , value); }
        }

        private DateTime fechaInicio;

        public DateTime FechaInicio
        {
            get { return fechaInicio; }
            set {SetProperty(ref fechaInicio , value); }
        }

        private DateTime fechaFin;

        public DateTime FechaFin
        {
            get { return fechaFin; }
            set {SetProperty(ref fechaFin , value); }
        }


        #endregion
        #region Variables

        GenericRepository<CategoriaGastos> _categoriaGastos;
        GenericRepository<CategoriaEntradas> _categoriaEntradas;
        GenericRepository<Gastos> _gastos;
        GenericRepository<Entrada> _entradas;


        #endregion
        public ActivoPasivoViewModel(string param)
        {
            CrearCommand = new Command(CrearCommandExecute);
            ModificarCommand = new Command(ModificarCommandExecte);
            EliminarCommand = new Command(EliminarCommandExecute);
            BuscarCommand = new Command(BuscarCommandExecute);
            Title = param;
            FechaInicio = DateTime.Now;
            FechaFin = DateTime.Now.AddDays(1);
            _categoriaGastos = new GenericRepository<CategoriaGastos>();
            _categoriaEntradas = new GenericRepository<CategoriaEntradas>();
            _gastos = new GenericRepository<Gastos>();
            _entradas = new GenericRepository<Entrada>();
            if (Title == "Entradas")
            {
                Categorias = _categoriaEntradas.Read.ToList<CategoriaBase>();
            }
            else
            {
                Categorias = _categoriaGastos.Read.ToList<CategoriaBase>();
            }
            DatoSelect = new ActivoPasivoBase();
            ActualizarTabla();
        }

        private async void BuscarCommandExecute()
        {
            if (FechaFin<=FechaInicio)
            {
                await Message.EnviarAlert("Verifique fechas", "Ok");
            }
            if (Title == "Entradas")
            {
                Datos = _entradas.Query(p=>p.FechaHoraCreacion>=FechaInicio && p.FechaHoraCreacion<=FechaFin.AddDays(1)).ToList<ActivoPasivoBase>();
            }
            else
            {
                Datos = _gastos.Query(p => p.FechaHoraCreacion >= FechaInicio && p.FechaHoraCreacion <= FechaFin.AddDays(1)).ToList<ActivoPasivoBase>();
            }
        }

        private void ActualizarTabla()
        {
            if (Title=="Entradas")
            {
                Datos = _entradas.Read.ToList<ActivoPasivoBase>();
            }
            else
            {
                Datos = _gastos.Read.ToList<ActivoPasivoBase>();
            }
        }

        private async void EliminarCommandExecute()
        {
            if (DatoSelect == null)
            {
                await Message.EnviarAlert("No se ha seleccionado un dato para modificar", "Ok");
            }
            if (Title == "Entradas")
            {
                if (_entradas.Delete(DatoSelect.Id))
                {
                    await Message.EnviarAlert("Entrada eliminada correctamente", "Aceptar");
                }
                else
                {
                    await Message.EnviarAlert("Error al eliminar entrada", "Aceptar");

                }
            }
            else
            {
                if (_gastos.Delete(DatoSelect.Id))
                {
                    await Message.EnviarAlert("Gasto eliminado correctamente", "Aceptar");
                }
                else
                {
                    await Message.EnviarAlert("Error al eliminar gasto", "Aceptar");

                }
            }
            ActualizarTabla();
        }

        private async void ModificarCommandExecte()
        {
            if (DatoSelect==null)
            {
                await Message.EnviarAlert("No se ha seleccionado un dato para modificar", "Ok");
            }
            if (CategoriaSelect == null)
            {
                await Message.EnviarAlert("No se ha seleccionado una categoria", "Ok");
            }
            if (Title == "Entradas")
            {
                if (_entradas.Update(new Entrada() { Descripcion = DatoSelect.Descripcion, IdCategoria = DatoSelect.IdCategoria, Monto = DatoSelect.Monto, Id = DatoSelect.Id, FechaHoraCreacion = DatoSelect.FechaHoraCreacion }) != null)
                {
                    await Message.EnviarAlert("Entrada modificada correctamente", "Aceptar");
                }
                else
                {
                    await Message.EnviarAlert("Error al modificar entrada", "Aceptar");

                }
            }
            else
            {
                if (_gastos.Update(new Gastos() { Descripcion = DatoSelect.Descripcion, IdCategoria = DatoSelect.IdCategoria, Monto = DatoSelect.Monto, Id=DatoSelect.Id, FechaHoraCreacion=DatoSelect.FechaHoraCreacion }) != null)
                {
                    await Message.EnviarAlert("Gasto creado correctamente", "Aceptar");
                }
                else
                {
                    await Message.EnviarAlert("Error al modifica gasto", "Aceptar");

                }
            }
            ActualizarTabla();
        }

        private async void CrearCommandExecute()
        {
            if (CategoriaSelect==null)
            {
              await  Message.EnviarAlert("No se ha seleccionado una categoria", "Ok");
            }
            if (Title == "Entradas")
            {
                if (_entradas.Create(new Entrada() { Descripcion=DatoSelect.Descripcion, IdCategoria=categoriaSelect.Id, Monto=DatoSelect.Monto })!=null)
                {
                    await Message.EnviarAlert("Entrada creada correctamente", "Aceptar");
                }
                else
                {
                    await Message.EnviarAlert("Error al crear entrada", "Aceptar");

                }
            }
            else
            {
                if (_gastos.Create(new Gastos() { Descripcion = DatoSelect.Descripcion,  IdCategoria = categoriaSelect.Id, Monto = DatoSelect.Monto }) != null)
                {
                    await Message.EnviarAlert("Gasto creado correctamente", "Aceptar");
                }
                else
                {
                    await Message.EnviarAlert("Error al crear gasto", "Aceptar");

                }
            }
            ActualizarTabla();
        }
    }
}

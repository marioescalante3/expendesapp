﻿using ExpensesApp.DAL;
using ExpensesApp.Entidades;
using ExpensesApp.Tools;
using ExpensesApp.View;
using Microcharts;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace ExpensesApp.ViewModel
{
    public class InicioViewModel : BaseViewModel
    {
        #region Command

        public ICommand ActivoPasivoCommand { get; set; }
        public ICommand CrearCategoriaCommand { get; set; }

        #endregion
        #region FullProperties

        private Chart chartEntradas;

        public Chart ChartEntradas
        {
            get { return chartEntradas; }
            set { SetProperty(ref chartEntradas, value); }
        }

        private Chart chartSalidas;

        public Chart ChartSalidas
        {
            get { return chartSalidas; }
            set { SetProperty(ref chartSalidas, value); }
        }

        #endregion

        #region Variables

        GenericRepository<CategoriaGastos> _categoriaGastos;
        GenericRepository<CategoriaEntradas> _categoriaEntradas;
        GenericRepository<Gastos> _gastos;
        GenericRepository<Entrada> _entradas;

        #endregion

        public InicioViewModel()
        {
            ActivoPasivoCommand = new Command<string>(param => CrearEntradaCommandExecute(param));
            CrearCategoriaCommand = new Command<string>(param => CraearCategoria(param));
            _categoriaEntradas = new GenericRepository<CategoriaEntradas>();
            _categoriaGastos = new GenericRepository<CategoriaGastos>();
            _entradas = new GenericRepository<Entrada>();
            _gastos = new GenericRepository<Gastos>();
            CrearGraficos();
        }

        internal void CrearGraficos()
        {
            ChartEntradas = new Microcharts.DonutChart() { Entries = CrearDatos("Entradas") };
            ChartSalidas = new Microcharts.DonutChart() { Entries = CrearDatos("Salidas") };
        }

        private IEnumerable<Microcharts.Entry> CrearDatos(string v)
        {
            List<Microcharts.Entry> datos = new List<Microcharts.Entry>();
            if (v == "Entradas")
            {
                foreach (CategoriaEntradas categoriaEntrada in _categoriaEntradas.Read)
                {
                    float suma = (float)_entradas.Query(p => p.IdCategoria == categoriaEntrada.Id).Sum(r => r.Monto);
                    datos.Add(new Microcharts.Entry(suma) { Label = categoriaEntrada.Name, ValueLabel = suma.ToString(), Color = SKColor.Parse(categoriaEntrada.Color), TextColor = SKColor.Parse(categoriaEntrada.Color) });
                }
            }
            else
            {
                foreach (CategoriaGastos categoriaGastos in _categoriaGastos.Read)
                {
                    float suma = (float)_gastos.Query(p=>p.IdCategoria==categoriaGastos.Id).Sum(r => r.Monto);
                    datos.Add(new Microcharts.Entry(suma) { Label = categoriaGastos.Name, ValueLabel = suma.ToString(), Color = SKColor.Parse(categoriaGastos.Color), TextColor = SKColor.Parse(categoriaGastos.Color) });
                }
            }
            return datos;
        }


        private void CraearCategoria(string param)
        {
            Navegacion(new View.CrearCategoria(param));
        }
        private void CrearEntradaCommandExecute(string param)
        {
            Navegacion(new ActivoPasivoView(param));
        }




    }
}

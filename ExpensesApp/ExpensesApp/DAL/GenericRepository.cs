﻿using ExpensesApp.Entidades;
using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace ExpensesApp.DAL
{
    public class GenericRepository<T>  where T : BaseDTO
    {
        string dbName;
        public GenericRepository()
        {
            dbName = Environment.GetFolderPath
                (Environment.SpecialFolder.LocalApplicationData) + "\\ExpensesApp.db";
        }
        public List<T> Read
        {
            get
            {
                List<T> datos;
                using (var db = new LiteDatabase(new ConnectionString() { Filename = dbName }))
                {
                    datos = db.GetCollection<T>(typeof(T).Name).FindAll().ToList();
                }
                // select * from Productos
                return datos;
            }
        }
        public T Create(T entidad)
        {
            try
            {
                entidad.Id = Guid.NewGuid().ToString();
                entidad.FechaHoraCreacion = DateTime.Now;
                using (var db = new LiteDatabase(new ConnectionString() { Filename = dbName }))
                {
                    db.GetCollection<T>(typeof(T).Name).Insert(entidad);
                }
                return entidad;
            }
            catch (Exception)
            {

                return null;
            }
        }
        public T Update(T entidad)
        {
            try
            {
                using (var db = new LiteDatabase(new ConnectionString() { Filename = dbName }))
                {
                    db.GetCollection<T>(typeof(T).Name).Update(entidad);
                }
                return entidad;
            }
            catch (Exception ex)
            {

                return null;
            }
        }
        public bool Delete(string id)
        {
            try
            {
                using (var db = new LiteDatabase(new ConnectionString() { Filename = dbName }))
                {
                    db.GetCollection<T>(typeof(T).Name).Delete(id);
                }
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }
        public T SearchById(string id)
        {
            //return Query(e => e.Id == id).SingleOrDefault(); hace lo mismo que el metodo de abajo
            return Read.Where(g => g.Id == id).SingleOrDefault();
        }
        public List<T> Query(Expression<Func<T, bool>> predicate)
        {
            return Read.Where(predicate.Compile()).ToList();
        }
    }
}

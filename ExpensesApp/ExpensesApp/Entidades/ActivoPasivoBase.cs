﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExpensesApp.Entidades
{
    public class ActivoPasivoBase:BaseDTO
    {
        public string IdCategoria { get; set; }
        public string Descripcion { get; set; }
        public decimal Monto { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExpensesApp.Entidades
{
   public class CategoriaGastos:CategoriaBase
    {
        public override string ToString()
        {
            return Name;
        }
    }
}

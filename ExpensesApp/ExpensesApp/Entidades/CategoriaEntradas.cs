﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExpensesApp.Entidades
{
    public class CategoriaEntradas:CategoriaBase
    {
        public override string ToString()
        {
            return Name;
        }
    }
}

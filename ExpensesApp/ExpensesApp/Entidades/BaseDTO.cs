﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExpensesApp.Entidades
{
   public class BaseDTO
    {
        public string Id { get; set; }
        public DateTime FechaHoraCreacion { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExpensesApp.Entidades
{
  public  class CategoriaBase:BaseDTO
    {
        public string Name { get; set; }
        public string Color { get; set; }
    }
}

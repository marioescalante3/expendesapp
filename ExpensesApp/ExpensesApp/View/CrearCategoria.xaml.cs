﻿using ExpensesApp.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ExpensesApp.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CrearCategoria : ContentPage
    {
        public CrearCategoria(string categoria)
        {
            InitializeComponent();
            BindingContext = new CategoriaViewModel(categoria);
            (BindingContext as CategoriaViewModel).NavegacionEvent += CrearCategoria_NavegacionEvent;
        }

        private void CrearCategoria_NavegacionEvent(object sender, Page e)
        {
            Navigation.PushAsync(e, true);
        }
    }
}
﻿using ExpensesApp.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ExpensesApp.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Inicio : ContentPage
    {
        public Inicio()
        {
            InitializeComponent();
            BindingContext = new InicioViewModel();
            (BindingContext as InicioViewModel).NavegacionEvent += Inicio_NavegacionEvent;
        }


        private void Inicio_NavegacionEvent(object sender, Page e)
        {
            Navigation.PushAsync(e, true);
        }

        protected override void OnAppearing()
        {
            (BindingContext as InicioViewModel).CrearGraficos();
        }
    }
}